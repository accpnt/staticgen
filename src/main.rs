use markdown;
use mustache::{MapBuilder, Template};
use glob::glob;
use std::{
    fs,
    collections::HashMap,
    io::{BufWriter, Write},
    str
};
use fs::File;
use chrono::{NaiveDate, Datelike};
use extract_frontmatter::{Extractor, config::Splitter};
use plotly::{Plot, Bar};
use serde::Deserialize;
use minify_html::{Cfg, minify};


#[derive(Debug, Deserialize)]
struct Config {
    domain: String,
    prefix: String,
    title: String,
    description: String,
    output: String
}

#[derive(Debug, Deserialize)]
struct Frontmatter {
    title: String,
    tags: String,
}

#[derive(Debug, Eq, Ord, PartialEq, PartialOrd, Clone)]
struct Post {
    path: String,
    date: NaiveDate,
    title: String,
    tags: String,
    content: String
}

struct Templates {
    collapsible: Template,
    homepage: Template,
    list: Template,
    navigation: Template,
    post: Template,
    stats: Template,
    tag: Template
}

fn write_html(file: &str, html: &str) {
    let write_file = File::create(file).unwrap();
    let mut writer = BufWriter::new(&write_file);
    writer.write_all(html.as_bytes()).unwrap();
}

fn select_top_tags(tag_counts: &HashMap<String, u32>, threshold: u32) -> HashMap<String, u32> {
    let mut top_tags_counts: HashMap<String, u32> = HashMap::new();
    let mut others_count = 0;

    // Sort tags by count in descending order
    let mut sorted_tags: Vec<(&String, &u32)> = tag_counts.iter().collect();
    sorted_tags.sort_by_key(|&(_, count)| count);
    sorted_tags.reverse();

    // Calculate counts for top tags and others
    for (tag, &count) in &sorted_tags {
        if count >= threshold {
            top_tags_counts.insert(tag.to_string(), count);
        } else {
            others_count += count;
        }
    }

    // add count for 'Others'
    //top_tags_counts.insert("others".to_string(), others_count);

    top_tags_counts
}


fn generate_plot(config: &Config, config_minified: &Cfg, posts: &Vec<Post>, templates: &Templates) {
    // create a date vector
    let mut yearly_counts = HashMap::<String, u32>::new();
    let mut monthly_counts = HashMap::<String, u32>::new();
    let mut tags_count = HashMap::<String, u32>::new();

    for post in posts {
        // Yearly count
        let y = post.date.format("%Y").to_string();
        *yearly_counts.entry(y.clone()).or_insert(0) += 1;

        // Monthly count
        let m = post.date.format("%Y-%m").to_string();
        *monthly_counts.entry(m.clone()).or_insert(0) += 1;

        let tags = post.tags.split(", ");
        for tag in tags {
            *tags_count.entry(tag.to_string()).or_insert(0) += 1;
        }
    }

    // select top tags and mark others
    let threshold = 8; // Adjust threshold as needed
    let top_tags_counts = select_top_tags(&tags_count, threshold);
    let mut sorted_top_tags: Vec<(&String, &u32)> = top_tags_counts.iter().collect();
    sorted_top_tags.sort_by(|a, b| b.1.cmp(a.1));

    // extract tags and counts
    let tags: Vec<String> = sorted_top_tags.iter().map(|&(tag, _)| tag.clone()).collect();
    let counts: Vec<u32> = sorted_top_tags.iter().map(|&(_, count)| *count).collect();
    let tags_count_trace = Bar::new(tags, counts);
    let mut tags_count_plot = Plot::new();
    tags_count_plot.add_trace(tags_count_trace);
    let tags_count_plot_html = tags_count_plot.to_inline_html(Some("tags_count"));

    let year_dates = yearly_counts.keys().cloned().collect();
    let year_count = yearly_counts.values().cloned().collect();
    let yearly_trace = Bar::new(year_dates, year_count);

    let month_dates = monthly_counts.keys().cloned().collect();
    let month_count = monthly_counts.values().cloned().collect();
    let monthly_trace = Bar::new(month_dates, month_count);

    let mut yearplot = Plot::new();
    yearplot.add_trace(yearly_trace);
    let yearplot_html = yearplot.to_inline_html(Some("year_count"));

    let mut monthplot = Plot::new();
    monthplot.add_trace(monthly_trace);
    let monthplot_html = monthplot.to_inline_html(Some("month_count"));

    let data = MapBuilder::new()
        .insert_str("title", &config.title)
        .insert_str("description", &config.description)
        .insert_str("year_plot", &yearplot_html)
        .insert_str("month_plot", &monthplot_html)
        .insert_str("tags_plot", &tags_count_plot_html)
        .build();

    // generate html for plot
    let plot_html = templates.stats.render_data_to_string(&data).unwrap();
    let minified_html = minify(&plot_html.as_bytes(), &config_minified);

    let index_file = format!("{}/stats.html", config.output);
    write_html(&index_file, str::from_utf8(&minified_html).expect("Success"));
}

fn generate_tags(config: &Config, config_minified: &Cfg, posts: &Vec<Post>, templates: &Templates) {
    let mut hm = HashMap::<String, Vec<Post>>::new();

    // uses HashMap entry instead of a for loop
    for post in posts {
        let post_tags = post.tags.split(", ");
        for tag in post_tags {
            hm.entry(tag.to_string())
                .or_insert_with(Vec::new)
                .push(post.clone());
        }
    }

    // create tag directories and pages
    for (key, value) in hm {
        let tag_dir = format!("{}/tag/{}", config.output, key);
        fs::create_dir(tag_dir).expect("Unable to create the directory");

        let post_list_html = generate_post_list(&value, templates);

        let data = MapBuilder::new()
            .insert_str("base", &config.domain)
            .insert_str("title", &config.title)
            .insert_str("description", &config.description)
            .insert_str("tag", &key)
            .insert_str("posts", &post_list_html)
            .build();

        // generate html for current post
        let tag_html = templates.tag.render_data_to_string(&data).unwrap();
        let minified_html = minify(&tag_html.as_bytes(), &config_minified);
        let index_file = format!("{}/tag/{}/index.html", config.output, key);
        fs::write(&index_file, &minified_html).expect("Unable to write file");
    }
}

fn populate_posts() -> Vec<Post> {
    let mut posts: Vec<Post> = Vec::new();
    
    // Use map instead of for loop to read file paths and then unwrap the results using collect() method
    let md_entries = glob("../posts/**/*.md")
        .expect("Failed to read glob pattern")
        .map(|entry| entry.unwrap());

    // map removes the use of match statement to filter out the errors.
    for md_path in md_entries {

        // retrieve file content as string
        let input = fs::read_to_string(md_path.clone()).expect("Unable to read the file");

        // declare frontmatter extractor
        let (frontmatter, content) = Extractor::new(Splitter::EnclosingLines("---")).extract(&input);
        
        // process frontmatter as YAML
        let frontmatter = frontmatter.to_string();
        let frontmatter: Frontmatter = serde_yaml::from_str(&frontmatter).expect("Could not read frontmatter");

        let filename = md_path.with_extension("html").file_name().unwrap().to_string_lossy().to_string();

        // create date object
        let naive_date = NaiveDate::parse_from_str(&filename[..10], "%Y_%m_%d").unwrap();

        // create post
        let post = Post{path: filename,
            date: naive_date,
            title: frontmatter.title,
            tags: frontmatter.tags,
            content: content.trim().to_string()};

        // add post to vector
        posts.push(post);
    }

    // sort by date
    posts.sort_by(|a, b| b.date.cmp(&a.date));

    return posts;
}

fn generate_tags_list(post: &Post) -> String {
    let mut tags_html: String = "TAGS: ".to_owned();
    let post_tags = post.tags.split(", ");
    for tag in post_tags {
        tags_html += &format!("<a href=\"tag/{}\">{}</a>, ", &tag, &tag);
    }

    // remove the two trailing ", " characters
    tags_html = tags_html[0..tags_html.len()-2].to_string();

    return tags_html;
}

fn generate_collapsible(templates: &Templates, begin: bool, year: &i32) -> String {
    let data = MapBuilder::new()
        .insert_bool("collapsible", begin)
        .insert_str("year", year.to_string())
        .build();

    let mut template_html = String::new();
    template_html += &templates.collapsible.render_data_to_string(&data).unwrap();
    return template_html;   
}

fn generate_post_list(posts: &Vec<Post>, templates: &Templates) -> String {
    let mut posts_html = String::new();

    let mut year = posts[0].date.year();

    posts_html.push_str(&generate_collapsible(templates, true, &year));

    for post in posts {
        let path = format!("posts/{}", &post.path);

        if year != post.date.year() {
            year = post.date.year();
            posts_html += &generate_collapsible(templates, false, &year);
            posts_html += &generate_collapsible(templates, true, &year);
        } 

        let tags_html = &generate_tags_list(&post);

        let data = MapBuilder::new()
            .insert_str("path", path)
            .insert_str("title", &post.title)
            .insert_str("tags", tags_html)
            .insert_str("date", &post.date.format("%d/%m/%Y").to_string())
            .build();

        // generate html for current post
        posts_html += &templates.list.render_data_to_string(&data).unwrap();
    }

    // end the collapsible
    posts_html += &generate_collapsible(templates, false, &year);

    return posts_html;
}

fn generate_post(config: &Config, config_minified: &Cfg, templates: &Templates, post: &Post, nav_html: String) {
    // convert markdown to html
    let content = markdown::to_html(&post.content);

    let tags_html = generate_tags_list(&post);

    // create blog path
    let blog_path = config.domain.to_string() + &config.prefix.to_string();

    let data = MapBuilder::new()
        .insert_str("base", &config.domain) 
        .insert_str("blog_title", &config.title)
        .insert_str("blog_path", &blog_path)
        .insert_str("post_title", &post.title)
        .insert_str("description", &config.description)
        .insert_str("content", content)
        .insert_str("navigation", nav_html)
        .insert_str("tags_list", tags_html)
        .insert_str("date", &post.date.format("%d/%m/%Y").to_string())
        .build();

    // generate html for current post
    let post_html = templates.post.render_data_to_string(&data).unwrap();
    let minified_html = minify(&post_html.as_bytes(), &config_minified);
    let index_file = format!("{}/posts/{}", &config.output, post.path);
    write_html(&index_file, str::from_utf8(&minified_html).expect("Success"));
}

fn generate_navigation(templates: &Templates, p: &Post, n: &Post) -> String {

    let data = MapBuilder::new()
        .insert_str("ppath", &p.path)
        .insert_str("ptitle", &p.title)
        .insert_str("npath", &n.path)
        .insert_str("ntitle", &n.title)
        .build();

    // generate html for current post
    let nav_html = templates.navigation.render_data_to_string(&data).unwrap();

    return nav_html;
}

fn generate_posts(config: &Config, config_minified: &Cfg, posts: &Vec<Post>, templates: &Templates) {

    for (i, post) in posts.iter().enumerate() {
        let n = if i == 0 { post } else { &posts[i - 1] };
        let p = if i == posts.len() - 1 { post } else { &posts[i + 1] };

        let nav_html = generate_navigation(templates, p, n);

        generate_post(config, &config_minified, templates, post, nav_html);
    }
}

fn generate_homepage(config: &Config, config_minified: &Cfg, posts: &Vec<Post>, templates: &Templates) {
    let posts_list_html = generate_post_list(posts, &templates);

    let data = MapBuilder::new()
        .insert_str("base", &config.domain)
        .insert_str("title", &config.title)
        .insert_str("description", &config.description)
        .insert_str("posts", &posts_list_html)
        .build();

    // generate html for current post
    let posts_html = templates.homepage.render_data_to_string(&data).unwrap();
    let minified_html = minify(&posts_html.as_bytes(), &config_minified);
    let index_file = format!("{}/index.html", config.output.to_string());
    write_html(&index_file, str::from_utf8(&minified_html).expect("Success"));
}


fn load_template(filename: &str) -> Template {
    // open mustache post template
    let template_input = fs::read_to_string(filename).expect("Unable to read the file");

    let template = mustache::compile_str(&template_input).unwrap();

    return template;
}

fn main() {
    // load blog configuration
    let config_file = fs::read_to_string("../config.yaml").expect("Unable to read the configuration file");
    let config: Config = serde_yaml::from_str(&config_file).expect("Could not read values");

    // cleanup
    fs::remove_dir_all(&config.output).expect("Unable to remove the directory");
    fs::create_dir(&config.output).expect("Unable to create the directory");
    fs::create_dir("../public/posts").expect("Unable to create the directory");
    fs::create_dir("../public/tag").expect("Unable to create the directory");

    let templates: Templates = Templates {
        collapsible: load_template("./templates/collapsible.mustache"),
        homepage: load_template("./templates/homepage.mustache"),
        list: load_template("./templates/list.mustache"),
        navigation: load_template("./templates/navigation.mustache"),
        post: load_template("./templates/post.mustache"),
        stats: load_template("./templates/stats.mustache"),
        tag: load_template("./templates/tag.mustache")
    };

    // create minify config: all fields false by default
    let mut config_minified = Cfg::new();
    config_minified.ensure_spec_compliant_unquoted_attribute_values = false;
    config_minified.keep_closing_tags = false;
    config_minified.keep_html_and_head_opening_tags = false;
    config_minified.keep_spaces_between_attributes = false;
    config_minified.keep_comments = false;
    config_minified.keep_input_type_text_attr = false;
    config_minified.keep_ssi_comments = false;
    config_minified.preserve_brace_template_syntax = false;
    config_minified.preserve_chevron_percent_template_syntax = false;
    config_minified.minify_css = true;
    config_minified.minify_js = true;
    config_minified.remove_bangs = true;
    config_minified.remove_processing_instructions = true;

    // retrieve markdown posts and generate html files
    let posts = populate_posts();
    generate_posts(&config, &config_minified, &posts, &templates);
    generate_tags(&config, &config_minified, &posts, &templates);
    generate_homepage(&config, &config_minified, &posts, &templates);
    generate_plot(&config, &config_minified, &posts, &templates);
}
