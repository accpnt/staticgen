import shutil
import argparse
import css_html_js_minify
import markdown
import chevron

from frontmatter import Frontmatter
from pathlib import Path
from datetime import datetime

BLOG_TITLE = 'Petit Rouge'
BLOG_DESCRIPTION = 'Chroniques compilées depuis 2009.'

DOMAIN = ''
PREFIX_PATH = ''

POSTS_INPUT_DIR = 'posts/'
POSTS_OUTPUT_DIR = 'public/'

ASSETS_INPUT_DIR = 'assets/'
ASSETS_OUTPUT_DIR = 'public/assets/'


def extract_year(post):
    d = datetime.strptime(post['attributes']['date'], '%Y-%m-%d')
    year = d.strftime('%Y')

    return year


def format_date(post):
    d = datetime.strptime(post['attributes']['date'], '%Y-%m-%d')
    date = d.strftime('%d/%m/%Y')

    return date


def generate_tags_list(post):
    tags_html = "TAGS: "
    tags = post['attributes']['tags'].split(', ')
    for tag in tags:
        tags_html += f'<a href="{DOMAIN}/{PREFIX_PATH}/tag/{tag}">{tag}</a>, '

    tags_html = tags_html[:-2]

    return tags_html


def generate_tags(posts):
    d = {}

    # collect tags in dictionary
    for post in posts:
        post_tags = post['attributes']['tags'].split(', ')
        for tag in post_tags:
            if tag in d:
                d[tag]['posts'].append(post)
            else:
                d[tag] = {'posts': [post]}

    # create tag root directory
    tag_root = Path(f'{POSTS_OUTPUT_DIR}/tag')
    tag_root.mkdir(parents=True, exist_ok=True)

    # create tags from dictionary
    for i, k in enumerate(d.keys()):
        tag_dir = Path(f'{tag_root}/{k}')
        tag_dir.mkdir(parents=True, exist_ok=True)
        post_list_html = generate_post_list(d[k]['posts'])

        with open('0lxt/templates/tag.mustache', 'r') as f:
            tag_html = chevron.render(f, {'title': BLOG_TITLE,
                                          'description': BLOG_DESCRIPTION,
                                          'root_path': f'{DOMAIN}/{PREFIX_PATH}',
                                          'tag': k,
                                          'posts': post_list_html})

        (tag_dir / Path('index.html')).write_text(tag_html)


def generate_post(post, navigation):
    path = post['attributes']['path']

    # create full path
    slug = Path(f'{POSTS_OUTPUT_DIR}/{path}')

    post_dir = Path(f'{slug}')
    post_dir.mkdir(parents=True, exist_ok=True)

    title = post['attributes']['title']
    date = format_date(post)
    body = post['body']
    content = markdown.markdown(body)

    tags_html = generate_tags_list(post)

    with open('0lxt/templates/post.mustache', 'r') as f:
        post = chevron.render(f, {'blog_title': BLOG_TITLE,
                                  'blog_path': f"{DOMAIN}/{PREFIX_PATH}",
                                  'post_title': title,
                                  'description': BLOG_DESCRIPTION,
                                  'date': date,
                                  'tags_list': tags_html,
                                  'content': content, 
                                  'navigation': navigation})

    (post_dir / Path('index.html')).write_text(post)


def generate_post_list(posts):
    posts_html = '<ul class="posts">'
    year = 0

    year = extract_year(posts[0])
    posts_html += f'<p>{year}</p>'

    for post in posts:
        path = f"{DOMAIN}/{PREFIX_PATH}{post['attributes']['path']}"
        title = post['attributes']['title']
        date = format_date(post)

        if year != extract_year(post):
            year = extract_year(post)
            posts_html += f'<p>{year}</p>'

        tags_html = generate_tags_list(post)

        # generate list element
        with open('0lxt/templates/list.mustache', 'r') as f:
            posts_html += chevron.render(f, {'path': path,
                                             'title': title,
                                             'tags': tags_html,
                                             'date': date})

    posts_html += '</ul>'

    return posts_html


def generate_homepage(posts):
    posts_list_html = generate_post_list(posts)

    with open('0lxt/templates/homepage.mustache', 'r') as f:
        homepage = chevron.render(f, {'title': BLOG_TITLE,
                                      'description': BLOG_DESCRIPTION,
                                      'posts': posts_list_html})



    Path(f'{POSTS_OUTPUT_DIR}/index.html').write_text(homepage)
    shutil.copytree(ASSETS_INPUT_DIR, ASSETS_OUTPUT_DIR)


def populate_posts():
    # recurse all subdirectories for Markdown files
    post_files = list(Path(POSTS_INPUT_DIR).glob('**/*.md'))

    posts = []

    for file in post_files:
        post = Frontmatter.read_file(file)
        posts.append(post)

    # reorder list by descending date
    posts.sort(key=lambda x: datetime.strptime(x['attributes']['date'], '%Y-%m-%d'),
               reverse=True)

    return posts


def generate_navigation(p, n):
    if p is None:
        plink = ""
    else:
        ppath = f"{DOMAIN}/{PREFIX_PATH}{p['attributes']['path']}"
        ptitle = p['attributes']['title']
        plink = f'<a href="{ppath}">{ptitle}</a>'

    if n is None:
        nlink = ""
    else:
        npath = f"{DOMAIN}/{PREFIX_PATH}{n['attributes']['path']}"
        ntitle = n['attributes']['title']
        nlink = f'<a href="{npath}">{ntitle}</a>'

    navigation_html = f''
    # generate list element
    with open('0lxt/templates/navigation.mustache', 'r') as f:
        navigation_html += chevron.render(f, {'previous': plink,
                                              'next': nlink})

    return navigation_html


def generate_posts(posts):
    for i, post in enumerate(posts):

        n = None if i == 0 else posts[i - 1]
        p = None if i == len(posts) - 1 else posts[i + 1]

        navigation_html = generate_navigation(p, n)

        generate_post(post, navigation_html)


def generate_site():
    shutil.rmtree(POSTS_OUTPUT_DIR, ignore_errors=True)
    Path(POSTS_OUTPUT_DIR).mkdir(parents=True, exist_ok=True)

    posts = populate_posts()

    generate_posts(posts)
    generate_tags(posts)
    generate_homepage(posts)


def main():
    # parse arguments
    args = argparse.ArgumentParser()
    args.add_argument("-p", "--prefix", required=True, help="prefix path")
    args.add_argument("-d", "--domain", required=True, help="domain")
    args = vars(args.parse_args())

    # define global variables
    global PREFIX_PATH
    PREFIX_PATH = args['prefix']
    global DOMAIN
    DOMAIN = args['domain']

    generate_site()


if __name__ == '__main__':
    main()
