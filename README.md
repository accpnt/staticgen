# staticgen

## Overview

Written in Rust, using Mustache as templates. Statically generated. 

## Update submodule

From the top directory, type:

```git submodule update --remote --merge```

## Extra

List posts by descending size:

```find . -type f -ls | sort -r -n -k7```

